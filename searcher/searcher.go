package searcher

import (
	"io/ioutil"
	"sort"
	"strings"
)

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func checkString(s string) bool {
	if strings.Contains(s, "\n") && strings.Contains(s, "\t") {
		return true
	}
	return false
}

func rankByWordCount(wordFrequencies map[string]int) PairList {
	pl := make(PairList, len(wordFrequencies))
	i := 0
	for k, v := range wordFrequencies {
		pl[i] = Pair{k, v}
		i++
	}
	sort.Sort(sort.Reverse(pl))
	return pl
}

// Pair contains a file and number of occurrences of a given search
type Pair struct {
	File     string
	Quantity int
}

// PairList represents a slice of Pairs
type PairList []Pair

func (p PairList) Len() int           { return len(p) }
func (p PairList) Less(i, j int) bool { return p[i].Quantity < p[j].Quantity }
func (p PairList) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

// Searcher searches for a given word Word in folder Route
func Searcher(Word, Route string) PairList {
	if strings.Compare(Word, "") == 0 {
		return nil
	}

	word := strings.ToLower(Word)
	files, _ := ioutil.ReadDir(Route)
	m := make(map[string]int)

	for _, f := range files {
		if !f.IsDir() && ('.' != f.Name()[0]) {
			openFile, err := ioutil.ReadFile(Route + "/" + f.Name())
			check(err)

			ParserFile := strings.Fields(string(openFile))

			for _, iy := range ParserFile {
				if checkString(strings.ToLower(iy)) {
					continue
				}
				if strings.ToLower(iy) == word {
					_, state := m[f.Name()]
					if !state {
						m[f.Name()] = 1
					} else {
						m[f.Name()]++
					}
				}
			}
		}
	}

	mOrder := rankByWordCount(m)

	//Delete map
	for k := range m {
		delete(m, k)
	}
	m = nil

	return mOrder
}
